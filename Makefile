############################################################################### 
# General Purpose Microsoft Makefile
# 
# Copyright (C) 2018, Martin Tang
############################################################################### 

# Toolchain
AR=$(TARGET)ar
PP=$(TARGET)cpp
AS=$(TARGET)as
CC=$(TARGET)gcc
CX=$(TARGET)g++
DB=$(TARGET)gdb
OD=$(TARGET)objdump
OC=$(TARGET)objcopy
RC=windres
FL=flex
BS=bison
LD=$(TARGET)gcc
RM=del

# Configuration
ARFLAGS=cr
PPFLAGS=-Isrc\function -DUC -DWIN32 -DGCC_COMP -DWINVER=0X0501 \
				-D_WIN32_IE=0x0500 -D_WIN32_WINNT=0x0501
ASFLAGS=$(PPFLAGS)
CCFLAGS=$(PPFLAGS) -O3 -msse3
CXFLAGS=$(PPFLAGS) -O3 -msse3
DBFLAGS=
RCFLAGS=$(PPFLAGS)
FLFLAGS=
BSFLAGS=
LDFLAGS=-Lsrc\function -Lsrc\zlib -static -mwindows
EXFLAGS=

# Projects
OBJECT1=src\zlib\adler32.o \
				src\zlib\compress.o \
				src\zlib\crc32.o \
				src\zlib\deflate.o \
				src\zlib\gzio.o \
				src\zlib\infblock.o \
				src\zlib\infcodes.o \
				src\zlib\inffast.o \
				src\zlib\inflate.o \
				src\zlib\inftrees.o \
				src\zlib\infutil.o \
				src\zlib\trees.o \
				src\zlib\uncompr.o \
				src\zlib\zutil.o
OUTPUT1=src\zlib\libzlib.a

OBJECT2=src\function\calcdiag.o \
				src\function\calcrect.o \
				src\function\date.o \
				src\function\debug.o \
				src\function\ellipss.o \
				src\function\files2.o \
				src\function\line2.o \
				src\function\own_process.o \
				src\function\owntime.o \
				src\function\params.o \
				src\function\rect.o \
				src\function\utf8.o
OUTPUT2=src\function\libfunction.a

OBJECT3=src\design\calc.o \
				src\design\change.o \
				src\design\check.o \
				src\design\command.o \
				src\design\design.o \
				src\design\files.o \
				src\design\help.o \
				src\design\instance.o \
				src\design\mainloop.o \
				src\design\memory.o \
				src\design\mentor.o \
				src\design\menus.o \
				src\design\netlist.o \
				src\design\nets.o \
				src\design\pcb.o \
				src\design\resource\design.o
OUTPUT3=bin\design.exe

OBJECT4=src\libman\libman.o \
				src\libman\resource\libman.o
OUTPUT4=bin\libman.exe

OBJECT5=src\geom\calc.o \
				src\geom\calcdef.o \
				src\geom\calcrect.o \
				src\geom\command.o \
				src\geom\dialogs.o \
				src\geom\draw.o \
				src\geom\draw2.o \
				src\geom\draw3.o \
				src\geom\dxf.o \
				src\geom\edit.o \
				src\geom\files.o \
				src\geom\geom.o \
				src\geom\graphics.o \
				src\geom\help.o \
				src\geom\insdel.o \
				src\geom\mainloop.o \
				src\geom\memory.o \
				src\geom\menus.o \
				src\geom\movecomp.o \
				src\geom\polygon.o \
				src\geom\print.o \
				src\geom\select.o \
				src\geom\toets.o \
				src\geom\resource\geom.o
OUTPUT5=bin\geom.exe

OBJECT6=src\sch\calc.o \
				src\sch\calc2.o \
				src\sch\calcdef.o \
				src\sch\calcrect.o \
				src\sch\change.o \
				src\sch\check.o \
				src\sch\command.o \
				src\sch\dialogs.o \
				src\sch\draw.o \
				src\sch\draw2.o \
				src\sch\draw3.o \
				src\sch\dxf.o \
				src\sch\edit.o \
				src\sch\edit2.o \
				src\sch\files.o \
				src\sch\graphics.o \
				src\sch\help.o \
				src\sch\inscomp.o \
				src\sch\insdel.o \
				src\sch\mainloop.o \
				src\sch\memory.o \
				src\sch\menus.o \
				src\sch\messages.o \
				src\sch\move2.o \
				src\sch\movecomp.o \
				src\sch\print.o \
				src\sch\property.o \
				src\sch\sch.o \
				src\sch\select.o \
				src\sch\toets.o \
				src\sch\uservar.o \
				src\sch\resource\sch.o
OUTPUT6=bin\sch.exe

# Projects
OBJECT7=src\pcb\calc.o \
				src\pcb\calc2.o \
				src\pcb\calc3.o \
				src\pcb\calc4.o \
				src\pcb\calcdef.o \
				src\pcb\calcdiag.o \
				src\pcb\calcrect.o \
				src\pcb\command.o \
				src\pcb\dialogs.o \
				src\pcb\draw.o \
				src\pcb\draw2.o \
				src\pcb\draw3.o \
				src\pcb\dxf.o \
				src\pcb\edif.o \
				src\pcb\edit.o \
				src\pcb\edit2.o \
				src\pcb\files.o \
				src\pcb\font.o \
				src\pcb\gateswap.o \
				src\pcb\geomsave.o \
				src\pcb\gerber.o \
				src\pcb\gerber2.o \
				src\pcb\gerber3.o \
				src\pcb\graphics.o \
				src\pcb\help.o \
				src\pcb\import.o \
				src\pcb\InputGerber.o \
				src\pcb\insdel.o \
				src\pcb\mainloop.o \
				src\pcb\memory.o \
				src\pcb\menus.o \
				src\pcb\move2.o \
				src\pcb\move3.o \
				src\pcb\movecomp.o \
				src\pcb\nets.o \
				src\pcb\nets2.o \
				src\pcb\odb.o \
				src\pcb\odb2.o \
				src\pcb\paint.o \
				src\pcb\pcb.o \
				src\pcb\plot.o \
				src\pcb\polygon.o \
				src\pcb\print.o \
				src\pcb\qspline.o \
				src\pcb\rules.o \
				src\pcb\select.o \
				src\pcb\select2.o \
				src\pcb\select3.o \
				src\pcb\select4.o \
				src\pcb\settings.o \
				src\pcb\tar.o \
				src\pcb\toets.o \
				src\pcb\trace2.o \
				src\pcb\trace3.o \
				src\pcb\trace4.o \
				src\pcb\trace5.o \
				src\pcb\trace6.o \
				src\pcb\uservar.o \
				src\pcb\resource\pcb.o
OUTPUT7=bin\pcb.exe

# Summary
LIBRARY=-lfunction -lzlib -lcomctl32 -lcomdlg32
OBJECTS=$(OBJECT1) $(OBJECT2) $(OBJECT3) $(OBJECT4) $(OBJECT5) $(OBJECT6) $(OBJECT7)
OUTPUTS=$(OUTPUT1) $(OUTPUT2) $(OUTPUT3) $(OUTPUT4) $(OUTPUT5) $(OUTPUT6) $(OUTPUT7)
DEPENDS=$(OBJECTS:.o=.dep)
CLEANUP=$(OBJECTS) $(OUTPUTS) $(DEPENDS)

# Build Commands
all: $(OUTPUTS)

# Dependency
$(OUTPUT1) : $(OBJECT1)
$(OUTPUT2) : $(OBJECT2)
$(OUTPUT3) : $(OBJECT3)
$(OUTPUT4) : $(OBJECT4)
$(OUTPUT5) : $(OBJECT5)
$(OUTPUT6) : $(OBJECT6)
$(OUTPUT7) : $(OBJECT7)

run: $(OUTPUTS)
	@echo [EX] $<
	@$< $(EXFLAGS)

debug: $(OUTPUTS)
	@echo [DB] $<
	@$(DB) $(DBFLAGS) $<

clean:
	@echo [RM] $(CLEANUP)
	-@$(RM) $(CLEANUP)

# Standard Procedures
%.dep : %.S
	@$(PP) $(PPFLAGS) -MM -MT $(@:.dep=.o) -o $@ $<

%.dep : %.c
	@$(PP) $(PPFLAGS) -MM -MT $(@:.dep=.o) -o $@ $<

%.dep : %.cpp
	@$(PP) $(PPFLAGS) -MM -MT $(@:.dep=.o) -o $@ $<

%.dep : %.rc
	@$(PP) $(PPFLAGS) -MM -MT $(@:.dep=.o) -o $@ $<

%.dep : %.l
	@$(PP) $(PPFLAGS) -MM -MT $(@:.dep=.o) -o $@ $<

%.dep : %.y
	@$(PP) $(PPFLAGS) -MM -MT $(@:.dep=.o) -o $@ $<

%.o : %.S
	@echo [AS] $<
	@$(AS) $(ASFLAGS) -c -o $@ $<

%.o : %.c
	@echo [CC] $<
	@$(CC) $(CCFLAGS) -c -o $@ $<

%.o : %.cpp
	@echo [CX] $<
	@$(CX) $(CXFLAGS) -c -o $@ $<

%.o : %.rc
	@echo [RC] $<
	@$(RC) $(RCFLAGS) $< $@

%.c : %.l
	@echo [FL] $<
	@$(FL) $(FLFLAGS) -o $@ $<

%.c : %.y
	@echo [BS] $<
	@$(BS) $(BSFLAGS) -d -o $@ $<

%.a :
	@echo [AR] $@
	@$(AR) $(ARFLAGS) $@ $^

%.dll :
	@echo [LD] $@
	@$(LD) $(LDFLAGS) -o $@ $^ $(LIBRARY)

%.exe :
	@echo [LD] $@
	@$(LD) $(LDFLAGS) -o $@ $^ $(LIBRARY)

%.elf:
	@echo [LD] $@
	@$(LD) $(LDFLAGS) -o $@ $^ $(LIBRARY)

%.hex : %.exe
	@echo [OC] $@
	@$(OC) -O ihex $< $@

%.hex : %.elf
	@echo [OC] $@
	@$(OC) -O ihex $< $@

ifneq ($(MAKECMDGOALS),clean)
-include $(DEPENDS)
endif

# NodeSpace 电路设计

#### 介绍
RouteSpace 是开源的电路设计软件，其中包含原理图设计、印刷电路板制图功能。本
程序源自国外已经荒废的 PCB Elegance 项目，目前的开发计划包含：

1. 继续完成汉化工作
2. 修正其中不合理的路径访问设计
3. 修复各种问题、提升用户体验
4. 努力达到可以释放v1.0版本的发布条件

#### 编译方法

1.  安装 MingW + GNU Make
2.  使用 GIT 下载代码仓库
3.  在项目目录下输入 make，即可自动按照依赖关系进行编译及生成 exe。

#### 如何参与贡献

1.  Fork 本项目仓库
2.  新建用户自有分支
3.  提交代码
4.  新建 Pull Request

#### 联系我们

ＱＱ群组：681205564

联系邮件：martin AT vaosim.com

![输入图片说明](https://images.gitee.com/uploads/images/2019/1021/080344_cda50e0d_1582741.png "2dcode.png")

